#!/bin/bash

#    9  pkill kiosk-browser
xrandr -q
xinput --list

TOUCH_DEVICE="ILITEK ILITEK Multi-Touch"
xinput --list "${TOUCH_DEVICE}"

GUI="HDMI-0"
APP1="DP-4"
APP2="DP-2"

GUI_X="0"
GUI_Y="0"
GUI_W="1280"
GUI_H="800"

APP_X="${GUI_W}"
APP_Y="0"

APP_H="2160"
APP_W="3840"


xrandr --output "${GUI}" --primary --mode "${GUI_W}x${GUI_H}" --pos "${GUI_X}x${GUI_Y}" --rotate normal #--output "${APP1}" --off --output "${APP2}" --off
xinput map-to-output "${TOUCH_DEVICE}" "${GUI}"

xrandr --output "${APP1}" --mode "${APP_W}x${APP_H}" --pos "${APP_X}x${APP_Y}" --rotate normal
xrandr --output "${APP2}" --mode "${APP_W}x${APP_H}" --right-of "${APP1}" --rotate normal

xrandr -q


## Antialiasing
# 5   -   4x (4xMS)
nvidia-settings --assign FSAA=5 --assign FSAAAppControlled=0 --assign FSAAAppEnhanced=0


cd /I

touch-controller/start.sh &

pid0="$!"
echo "=> GUI PID: $pid0"
sleep 1
xwininfo -tree -root


## pid x y w h
function window_set() {
  echo "Running 'window_set' with the following arguments: [$@]!"
  PID="$1"
  shift
  sleep 1
  WID=`xdotool search --sync --onlyvisible --pid $PID 2>/dev/null`
  echo "PID: $PID => WID: $WID"
  xprop -id ${WID}
  echo "Moving the window to [$1 x $2]"
  xdotool windowmove ${WID} $1 $2
  shift
  shift
  sleep 1
  echo "Resizing the window to [$1 x $2]"
  xdotool windowsize -sync ${WID} $1 $2
}

window_set "$pid0" "${GUI_X}" "${GUI_Y}" "${GUI_W}" "${GUI_H}"


## name x y w h
function window_set_name() {
  echo "Running 'window_set_name' with the following arguments: [$@]!"
  PID="$1"
  shift
  sleep 1
  xwininfo -tree -root
  WID=`xdotool search --limit 1 --sync --onlyvisible --name "$PID" 2>/dev/null`
  echo "PID: $PID => WID: $WID"
  xprop -id ${WID}
  echo "Moving the window to [$1 x $2]"
  xdotool windowmove ${WID} $1 $2
  shift
  shift
  sleep 1
  echo "Resizing the window to [$1 x $2]"
  xdotool windowsize -sync ${WID} $1 $2
  xrandr -q
}

#GUIid=`xdotool search --sync --onlyvisible --pid $pid0 2>/dev/null`
#xdotool windowmove ${GUIid} ${GUI_X} ${GUI_Y}
#sleep 1
#xdotool windowsize -sync ${GUIid} ${GUI_W} ${GUI_H}
#1280 800


#/bin/bash -c "launch.sh browser.sh -z 1.0 -l file:///I/WebGL/webgl_SphereProjection.html?HB_APP_ID=${APP_ID}\&HB_URL=${HB_URL}" &
# /bin/bash -c "launch.sh browser.sh -z 1.0 -l file:///I/WebGL/webgl_MilkyWayPanorama.html?HB_APP_ID=${APP_ID}\&HB_URL=${HB_URL}" &
/bin/bash -c "hb_preload.sh browser.sh -z 1.0 -l file:///I/WebGL/webgl_MilkyWayPanorama.html?HB_APP_ID=${APP_ID}\&HB_URL=${HB_URL}" &

pid="$!"
echo "=> APP PID: $pid"
sleep 1
xwininfo -tree -root
#sleep 1
# as above

#APPid=`xdotool search --sync --onlyvisible --pid $pid 2>/dev/null`
# 0x1000001
#xdotool windowmove ${APPid} ${APP_X} ${APP_Y}
#sleep 1
#xdotool windowsize -sync ${APPid} ${APP_W} 10
#sleep 1
#xdotool windowsize -sync ${APPid} ${APP_W} ${APP_H}

N="Milky Way Galaxy"
window_set_name "$N" "${APP_X}" "${APP_Y}" $(($APP_W + $APP_W)) "${APP_H}"


xinput map-to-output "${TOUCH_DEVICE}" "${GUI}"

xwininfo -tree -root

wait -n $pid $pid0
exit 0

FROM hits/hitsfat:0.3.0-1

ARG DP_CREDENTIALS=none

WORKDIR /I
COPY WebGL /I/WebGL/
COPY touch-controller /I/touch-controller/
COPY kivy_config.ini /root/.kivy/config.ini
COPY 2in1.sh /I/

# get proprietary files (only possible for ESO Supernova)
RUN /usr/local/bin/replace-with-nonfree-files.sh \
        "https://supernova.eso.org/exhibition/storage/interactives_1007_optical-risinger-allsky.png" \
        "/I/WebGL/Images/milkyway/Optical-allsky.png" \
    && echo -n "Checking if image credit needs to be adjusted ..." \
    && { test -e "/I/WebGL/Images/milkyway/Optical-allsky.png.info" \
         && echo " yes." \
         && sed -e 's|Credit: ESO/S. Brunier|Credit: Nick Risinger|' -i.bak touch-controller/content_milky_way.yaml \
         || echo " no." ; }

ARG IMAGE_VERSION=latest
ARG GIT_NOT_CLEAN_CHECK
ARG BUILD_DATE=unknown
ARG VCS_REF=unknown
ARG VCS_DATE=unknown
ARG VCS_SUMMARY=unknown
ARG VCS_URL=unknown
ARG DOCKERFILE=unknown

LABEL maintainer="Volker Gaibler <volker.gaibler@h-its.org>" \
    org.label-schema.name="Milky Way Panorama" \
    org.label-schema.description="Interactive application for the ESO Supernova" \
    org.label-schema.vendor="HITS gGmbH" \
    org.label-schema.vcs-ref="${VCS_REF}" \
    org.label-schema.vcs-url="${VCS_URL}" \
    org.label-schema.version="${VCS_VERSION}" \
    org.label-schema.build-date="${BUILD_DATE}" \
    org.label-schema.schema-version="1.0" \
    VCS_DATE="${VCS_DATE}" \
    VCS_SUMMARY="${VCS_SUMMARY}" \
    IMAGE_VERSION="${IMAGE_VERSION}" \
    GIT_NOT_CLEAN_CHECK="${GIT_NOT_CLEAN_CHECK}" \
    DOCKERFILE="${DOCKERFILE}"

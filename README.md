# 1007 - Milky Way Panorama

## Short Description

<table align="center">
    <tr>
    <td align="left" style="font-style:italic; font-size:12px; background-color:white">Explore the Milky Way!<br>
        Use the touchscreen of the control station to look at the Milky Way at different wavelengths.<br>
        How does the view change? Get more information at the control station.</td>
    </tr>
</table>

The application shows a selection of panoramic views of the Milky Way as observed at different wavelengths. 
The visitor can explore different components of our Galaxy observable at different wavelengths by selecting the wavelength on the control station.

In the exhibition, the application spreads over a video wall consisting of two large 4k monitors. From the control station different wavelengths / frequencies can be chosen revealing different features of our sky. The control station offers more information about individual visualizations. The user also can choose to see some annotations, constellation lines or a grid of the sky. Two buttons in the lower left corner of the control station allow the user to restart the application or change the language (English or German). 

This application is used at the [ESO Supernova Planetarium and Visitor Centre](https://supernova.eso.org/?lang=en), Garching b. München.  
For more details about the project and how applications are run and managed within the exhibition please see [this link](https://gitlab.com/HITS_Supernova/overview).   

## Requirements / How-To

This station uses a computer with two Ultra HD screens next to each other, and a touch screen at a control console in front of the big screens to control the wavelength setting and display additional explanations (1280x800 pixels resolution for the touch screen). The start-up script `2in1.sh` arranges the windows on the displays, sets the resolution and starts the different software components of the station.

**WebGL**   
A browser with a WebGL support is needed to run the interactive (start `WebGL/webgl_MilkyWayPanorama.html`).  

**Touch Controller**  
The [Touch Controller](https://gitlab.com/HITS_Supernova/0000_touchcontroller) application is used to control the image or video shown on the projection with an additional touch screen. Communication with projection application (WebGL) happens through websockets. It is started by the wrapper script `touch-controller/start.sh`). Available textures are defined in `touch-controller/content_earth_moon.yaml`.

## Detailed Information

Keyboard keys (for testing):

    - test images: "1","2","3"   
    - test overlays: "7"/"4" grid on/off. "8"/"5" annotations on/off.  "9"/"6" constellations on/off    
    - reload: "r"  


#### Client/server communication 

WebSockets are used to communicate between the python GUI and the webpage.  

    Websockets port: 8889  
    Websockets example message: {"texture":imageSource, "overlay_grid":imageSource, "overlay_annotations":imageSource, "overlay_constellations":imageSource}  
    Websockets example reset message {"texture":"Reset"}

#### (Optional) Using the Additional Scripts

Applications within the [Hilbert](https://github.com/hilbert) Museum Management System are used as [Docker](https://www.docker.com/) containers.   
Dockerized version of the application uses the `Dockerfile`, `Makefile` and various bash scripts to properly start the station.      

Application uses the following scripts to start: 

* `docker-compose.hitsmilkywaypanorama.yml` (yml file not available):
    * starts `2in1.sh`:  
        * spreads the WebGL application over two screens, and Python GUI over a third touchscreen
        * starts python GUI (`touch-controller/start.sh`)   
        * starts WebGL application using Hilbert kiosk browser (`WebGL/webgl_MilkyWayPanorama.html`)
        
* `Dockerfile` additionally does the following:
    * uses Risinger or Brunier Optical Milky Way image 



## Credits

This application was developed by the ESO Supernova team at [HITS gGmbH](https://www.h-its.org/en/).  
Idea by Dorotea Dudas and Volker Gaibler, HITS gGmbH.  
WebGL image blend application by Dorotea Dudas.  
Touch controller application by Volker Gaibler.  
Additional image processing by Volker Gaibler and Dorotea Dudas.

#### Code Licensing 

* This code is licensed as: [MIT license](LICENSE)
* MIT license:
    * *Three.js* by Mr.doob (Ricardo Cabello) [source](https://threejs.org/)
    * *THREEx.KeyboardState.js* by Jerome Etienne [threex.keyboardstate](https://github.com/jeromeetienne/threex.keyboardstate)
    * *Image Blend shader* (intrinsic) by Dorotea Dudas 
    
#### Image Licensing  

* CC BY 4.0:
    * Optical Milky Way image: original file by [S. Brunier/ESO](https://www.eso.org/public/images/eso0932a/) at 6k resolution and Creative Commons (*Images/milkyway/Optical-Brunier-allsky-6kcleaned.png*). Cleaned and adjusted. ESO Supernova uses an image by Nick Risinger which is non-free and downloaded from protected source.
    * Milky Way in different wavelengths provided by ESO (9x i.e. 10x) (+ Polarization as test only)
    * Overlays: annotations, constellations, grid provided by ESO / HITS gGmbH
* HI images (*hips_radio_composite_21cm_hi4pi.png*) permission and credits cleared with B. Winkel.
* 408 MHz radio image (*Radio Radio_Haslam_408MHz_log10-250K.png*): retrieved from [LAMBDA/NASA](https://lambda.gsfc.nasa.gov/product/foreground/sos/lambda_haslam408_dsds_sos2048.png) and adjusted in color.

